package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.GET("/", index)
	e.GET("/sdc1/feProxy/onboarding-api/v1.0/items/:itemID/versions", getItemVersions)
	e.GET("/sdc1/feProxy/onboarding-api/v1.0/items/:itemID/versions/:versionID", getItemVersion)
	e.PUT("/sdc1/feProxy/onboarding-api/v1.0/items/:itemID/versions/:versionID/actions", updateItemVersion)
	e.GET("/sdc1/feProxy/onboarding-api/v1.0/vendor-license-models", getVendorServiceModels)
	e.POST("/sdc1/feProxy/onboarding-api/v1.0/vendor-license-models", postVendorServiceModels)
	e.PUT("/sdc1/feProxy/onboarding-api/v1.0/vendor-license-models/:vendorID/versions/:versionID/actions", updateVendorVersion)
	e.GET("/sdc1/feProxy/onboarding-api/v1.0/vendor-software-products", getVendorSoftwareProducts)
	e.POST("/sdc1/feProxy/onboarding-api/v1.0/vendor-software-products", postVendorSoftwareProducts)
	e.GET("/sdc1/feProxy/onboarding-api/v1.0/vendor-software-products/:vspID/versions/:versionID", getVspVersion)
	e.PUT("/sdc1/feProxy/onboarding-api/v1.0/vendor-software-products/:vspID/versions/:versionID/actions", updateVspVersion)
	e.POST("/sdc1/feProxy/onboarding-api/v1.0/vendor-software-products/:vspID/versions/:versionID/orchestration-template-candidate", uploadArtifacts)
	e.PUT("/sdc1/feProxy/onboarding-api/v1.0/vendor-software-products/:vspID/versions/:versionID/orchestration-template-candidate/process", validateArtifacts)
	e.POST("/reset", reset)
	generateInitialVendorList()
	generateInitialVspList()
	e.Logger.Fatal(e.Start(":30206"))
}
